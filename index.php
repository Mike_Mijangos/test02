<?php

session_start();
if(!isset($_SESSION["user"])){
  header("location:login.php");
}

echo '<div align=right><h4 >Bienvenido :'.$_SESSION["user"].'</h4>';
echo '<p><a href="logout.php">Cerrar sesión</a></p> </div>';

?>

<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="assests/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assests/datatables/datatables.min.css">  
  <script>
function myFunction() {
    window.print();
}
</script>	
</head> 
	<title>Listado de articulos</title>  	
</head>
<body>

	<center >
	<h1 class="page-header success"><img src="en.png" alt="" style="width:60px;height:52px;"> &nbsp ARTICULOS &nbsp Y &nbsp PRODUCTOS  &nbsp <img src="carro.png" alt="" style="width:156px;height:65px;"></h1> 
	</center>
				
    <div align="right">
     

<button onclick="myFunction()">Imprimir</button>

    </div>

	<div id="mess" class="container">
		<div class="row">
			<div class="col-md-12">

				<br />

				<table class="table" id="tablaPedidos">					
					<thead>
						<tr>
							<th>*</th>
							
							<th>ID_ART</th>										
							<th>PRODUCTO</th>
							<th>ID_MATERIAL_P_</th>
							<th>ID_MATERIAL_A_</th>
							<th>CANTIDAD</th>								
							<th>DESCRIPCIÓN</th>
							<th>UNIDAD</th>								
							<th>EXISTENCIA</th>
							<th>ID_BODEGA</th>
							<th>CANT_A_PEDIR</th>
							<th><a>EDITAR</a></th>
							<th><a>ELIMINAR</a></th>
							
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	
	<div class="modal fade" tabindex="-1" role="dialog" id="pedidoModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div id="" class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> <p id="">  </p> </h4>
	      </div>

		<form class="form-horizontal" action="" method="" id="pedidoForm">	      

	      <div class="modal-body">
	        	
	        <div class="edit-messages"></div>
	        			  
			    <div class="">
				    <div class="">
				    <div class="col-sm-4">
					      <div class="form-group">
			    <label for="" class="control-label">Id_Prov</label>
			    <div class="">
			     
			    	<input type="text" class="form-control" id="id_proveedor" name="proveedor" readonly="">
			    
			    </div>
			  </div>
					    </div>
					    <div class="col-sm-4">
					      <div class="form-group">
			    <label for="" class="control-label">Proveedor</label>
			    <div class="">
			     
			    	<input type="text" class="form-control" id="proveedor" name="proveedor" readonly="">
			    
			    </div>
			  </div>
					    </div>
					    
					    <div class="col-sm-4">
					      <div class="form-group">
			    <label for="" class="col-sm-2 control-label"> Estatus</label>
			    <div class="">
			      <input type="text" class="form-control" id="estatus" name="estatus" readonly="" >
			    </div>
			    </div>
					    </div>
				    </div>
				</div>
			    <div class="form-group">
			    <label for="" class="col-sm-2 control-label">Confiabilidad</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="confiabilidad" name="confiabilidad" readonly="">
			    </div>

			  </div>

			  <div class="form-group">
			    <label for="" class="col-sm-2 control-label">Id_Art</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="Id_Art" name="Id_Art" readonly="">
			    </div>

			  </div>

			  <div class="form-group">
			    <label for="" class="col-sm-2 control-label">Art requeridos</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="requeridos" name="requeridos" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

			    </div>

			  </div>

			  <div class="form-group">
			    <label for="" class="col-sm-2 control-label">Unidad</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="unidad" name="unidad" readonly="" >
			    </div>
			    
			  </div>
			  
			  <div class="form-group">
			    <label for="" class="col-sm-2 control-label">Precio unitario</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="unitario" id="unitario" readonly="">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="total" class="col-sm-2 control-label">Total</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="total" id="total" readonly="">
			    </div>
			  </div>
			  
 			<div class="form-group">
			    <label for="" class="col-sm-2 control-label">Días de entrega</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="entrega" id="entrega" readonly="">
			    </div>
			  </div>
			

	      <div class="modal-footer ">
	        <button type="button" id="cierre" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" onclick="insert()" class="btn btn-primary">Guardar</button>
	      </div>
	      </div>
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- / modal -->

	
	<script type="text/javascript" src="assests/jquery/jquery.min.js"></script>
	
	<script type="text/javascript" src="assests/bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript" src="assests/datatables/datatables.min.js"></script>
	
	<script type="text/javascript" src="custom/js/index.js"></script>

</body>
</html>