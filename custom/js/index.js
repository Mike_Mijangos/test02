// AL MOMENTO DE QUE ESTÁ LISTO EL DOCUMENTO, SE CARGA TODA LA TABLA

$(document).ready(function() {
	getTable();
});

//FUNCIÓN PARA POBLAR LA TABLA PRINCIPAL
var tablaPedidos;
function getTable(){

	tablaPedidos = $("#tablaPedidos").DataTable({
		"ajax": "php_action/getAllArticulos.php",
		"order": []		
	});
}




// FUNCIÓN QUE OBTIENE EL PROVEEDOR ACTIVO  Y LOS DATOS PARA EL FORMULARIO DEL PEDIDO
// $id : es el de material a comprar
// $ pedir : si es mayor a 0 significa que no hay que realizar un pedido debido a que si alcanza el material, es la cantidad a comprar 
function crearPedido(id = null, pedir = null) {
	if(id) {
		total('requeridos','unitario');

		//  error 
		$(".form-group").removeClass('has-error').removeClass('has-success');
		$(".text-danger").remove();
		$(".edit-messages").html("");


		$.ajax({
			url: 'php_action/getProveedor.php',
			type: 'post',
			data: {ID_ART : id, PEDIR : pedir},
			dataType: 'json',
			success:function(response) {

				$( "p" ).html('ID: ' + response[0].ID_ART + ' - ' + response[0].DESCRIPCION.toUpperCase());
				$("#unitario").val(response[0].PRECIO);
				$("#Id_Art").val(response[0].ID_ART);
				$("#entrega").val(response[0].DIAS_ENTREGA);
				
				$("#id_proveedor").val(response[0].ID_PROV);
				$("#proveedor").val(response[0].RAZON_SOCIAL);
				$("#estatus").val(response[0].ESTATUS);	
				$("#unidad").val(response[0].UNIDAD);	
				$("#confiabilidad").val(response[0].NIVEL);				
				$("#requeridos").val(response[1].PEDIR);							
				$("#observaciones").val(response.observaciones);
			
				
				$("#pedidoForm").unbind('submit')
					         	  .bind('submit', function() {					
					$(".text-danger").remove();
					return false;
				});				
			} // /success
		}); 

	} else {
		alert("Error");
	}
}

function eliminarPedido(id = null) {
	if(id) {
		
		$.ajax({
			url: 'php_action/delete.php',
			type: 'post',
			data: {ID_ART : id},
			dataType: 'json',
			success:function(data) {
		      if(data){
		      	tabla= $("#tablaPedidos").DataTable();
		      	//$("#tablaPedidos").destroy();
		      	tabla.destroy();
		 	    getTable();	
		      }
			} // /success
		}); 

	} else {
		alert("Error");
	}
}

// FUNCION QUE ESCUCHA LOS EVENTOS DEL TECLADO, SIRVE PARA CALCULAR EL MONTO TOTAL DEL PEDIDO, SOLO ACEPTA NÚMEROS

function total(cantidad, importe){
  	$("#" + cantidad )
  		.keyup(function() {
    	var a = $( this ).val();
    	var b = $("#" + importe).val();
    	var t = a*b
    	console.log(a + ' * ' + b  + '=' +  t);
    	$( "#total" ).val( t );

  	})
  	.keyup();
}

//FUNCIÓN PARA GUARDAR EN LA BASE LOS DATOS OBTENIDOS DEL FORMULARIO
function insert(){
	id_art = $('#Id_Art').val();
	cantidad = $('#requeridos').val();
	unidad  = $('#unidad').val();
	proveedor = $('#id_proveedor').val();
	importe = $('#total').val();
	entrega = $('#entrega').val();
	
	$.ajax({
		url: 'php_action/create.php',
		type: 'post',
		data: {ID_ART:id_art, CANTIDAD:cantidad,  UNIDAD:unidad, PROVEEDOR:proveedor, IMPORTE:importe, ENTREGA:entrega },
		dataType: 'json',
		success:function(response) {
			if(response.success) {
				$(".edit-messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
				  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
				  '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
				'</div>');
				
				$(".form-group").removeClass('has-success').removeClass('has-error');
				$(".text-danger").remove();
			} else {
				$(".edit-messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
				  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
				  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
				'</div>')
			}
		} // /success
						}); // /ajax

}