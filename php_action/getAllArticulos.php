<?php 

require_once 'db_connect.php';

$output = array('data' => array());

// SE OBTIENEN LAS ESTRUCTURAS Y SUS RESPECTIVOS MATERIALES, SE SE CALCÚLA EL MATERIAL NECESARIO PARA CREAR LA ESTRUCTURA Y SE VERIFICA EN ALMACEN LA EXISTENCIA DE DICHOS MATERIALES.
 
$sql = "SELECT 
    a.ID_ART ,
    a.DESCRIPCION AS PRODUCTO_TERMINADO,
    a.ID_MATERIAL_PRIMARIO,
    a.ID_MATERIAL_ALTERNO,
    a.CANTIDAD,
    ar.DESCRIPCION,
    ar.UNIDAD,
    Al.EXISTENCIA,
    Al.ID_BODEGA,
    IF(a.CANTIDAD > Al.EXISTENCIA,
        TRUE,
        FALSE) LEVANTAR_PEDIDO,
    Al.EXISTENCIA - a.CANTIDAD AS RESTANTES, T.PEDIR
FROM
    (SELECT 
        a.ID_ART,
            a.DESCRIPCION,
            ae.ID_ART AS ID_ART_C,
            ae.ID_ESTRUCTURA AS ID_ESTRUCTURA_C,
            e.ID_MATERIAL_PRIMARIO,
            e.ID_MATERIAL_ALTERNO,
            e.CANTIDAD
    FROM
        Articulos a
    INNER JOIN Articulo_Estructura ae ON a.ID_ART = ae.ID_ART
    INNER JOIN Estructura e ON ae.ID_ESTRUCTURA = e.ID_ESTRUCTURA
    -- WHERE
     --   a.ID_ART = 8
        ) a
        LEFT JOIN
    Articulos ar ON a.ID_MATERIAL_PRIMARIO = ar.ID_ART
        OR a.ID_MATERIAL_ALTERNO = ar.ID_ART
        INNER JOIN
    Almacen al ON a.ID_MATERIAL_PRIMARIO = al.ID_ART
        OR a.ID_MATERIAL_ALTERNO = al.ID_ART
        -- ------------
        LEFT JOIN (
        
        SELECT 
    a.ID_ART AS ID,
    a.DESCRIPCION AS PRODUCTO_TERMINADO,
    a.ID_MATERIAL_PRIMARIO,
    a.ID_MATERIAL_ALTERNO,
    a.CANTIDAD,
    ar.DESCRIPCION,
    ar.UNIDAD,
    SUM(Al.EXISTENCIA) AS EXISTENCIA,
    Al.ID_BODEGA,
    IF(a.CANTIDAD > Al.EXISTENCIA,
        TRUE,
        FALSE) LEVANTAR_PEDIDO,
    (SUM(Al.EXISTENCIA) - SUM(a.CANTIDAD)) AS RESTANTES
    , SUM(Al.EXISTENCIA)  - (SUM(Al.EXISTENCIA) - SUM(a.CANTIDAD)) AS PEDIR -- IF (PEDIR <0 ) PEDIR Y SACAR LO QUE NECESITA POR PIEZA E INSERTARLO, SINO NO HACER NADA.
FROM
    (SELECT 
        a.ID_ART,
            a.DESCRIPCION,
            ae.ID_ART AS ID_ART_C,
            ae.ID_ESTRUCTURA AS ID_ESTRUCTURA_C,
            e.ID_MATERIAL_PRIMARIO,
            e.ID_MATERIAL_ALTERNO,
            e.CANTIDAD
    FROM
        Articulos a
    INNER JOIN Articulo_Estructura ae ON a.ID_ART = ae.ID_ART
    INNER JOIN Estructura e ON ae.ID_ESTRUCTURA = e.ID_ESTRUCTURA
    -- WHERE
     --   a.ID_ART = 8
        ) a
        LEFT JOIN
    Articulos ar ON a.ID_MATERIAL_PRIMARIO = ar.ID_ART
        OR a.ID_MATERIAL_ALTERNO = ar.ID_ART
        INNER JOIN
    Almacen al ON a.ID_MATERIAL_PRIMARIO = al.ID_ART
        OR a.ID_MATERIAL_ALTERNO = al.ID_ART
        GROUP BY 1) T ON a.ID_ART = T.ID;";

$query = $connect->query($sql);

$x = 1;
$aux = $aux2 = $id = $compra = $pedir =  0;
$levantarPedido = "";
while ($row = $query->fetch_assoc()) {

	$aux = $row['ID_MATERIAL_PRIMARIO'];
	$aux2 = $row['ID_MATERIAL_ALTERNO'];
	$id = $aux > $aux2 ? $aux : $aux2;
    $compra = ($row['RESTANTES']);
    $pedir = ($row['RESTANTES']) >= 0 ? '0': abs($row['RESTANTES']);
   // $levantarPedido = $row['LEVANTAR_PEDIDO'] == 0 ? 'N/A' : 'SI';
    
	$actionButton =  $pedir <> 100 ? '                                             
	<li><a type="button" data-toggle="modal" data-target="#pedidoModal" onclick="crearPedido('.$id.','.$pedir.')"> <span class="glyphicon glyphicon-pencil"></span></a></li>' : '';
	;
 
    $actionDelete =  $pedir <> 100 ? '                                              
    
    <a type="button" onclick="eliminarPedido('.$id.')"> <span class="glyphicon glyphicon-remove"></span></a>' : '';
    ;

	$output['data'][] = array(
		$x,
		$row['ID_ART'],
		$row['PRODUCTO_TERMINADO'],
		$row['ID_MATERIAL_PRIMARIO'],
		$row['ID_MATERIAL_ALTERNO'],
		$row['CANTIDAD'],
		$row['DESCRIPCION'],
		$row['UNIDAD'],
		$row['EXISTENCIA'],
		$row['ID_BODEGA'],
	//	$levantarPedido,
        $pedir,
		$actionButton
        ,$actionDelete
	);

	$x++;
}

$connect->close();

echo json_encode($output);