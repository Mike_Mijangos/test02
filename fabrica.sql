-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Mar 08, 2018 at 15:56 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `fabrica1`
--
CREATE DATABASE IF NOT EXISTS `fabrica1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fabrica1`;

-- --------------------------------------------------------

--
-- Table structure for table `almacen`
--
CREATE TABLE `usuario` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
   PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `usuario` VALUES (1,'mike','mike'),(2,'admin','admin');


CREATE TABLE `almacen` (
  `ID_ART` int(11) NOT NULL,
  `ID_BODEGA` int(11) DEFAULT NULL,
  `EXISTENCIA` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `almacen`
--

INSERT INTO `almacen` (`ID_ART`, `ID_BODEGA`, `EXISTENCIA`) VALUES
(1, 1, 10),
(6, 2, 1),
(2, 3, 29),
(3, 1, 4),
(4, 3, 9),
(5, 2, 1000),
(11, 1, 10),
(12, 2, 0),
(13, 1, 100),
(14, 2, 0),
(15, 1, 12),
(16, 2, 22),
(17, 1, 190),
(18, 1, 20),
(19, 3, 100),
(20, 2, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `articulos`
--

CREATE TABLE `articulos` (
  `ID_ART` int(11) NOT NULL,
  `DESCRIPCION` varchar(20) DEFAULT NULL,
  `UNIDAD` varchar(20) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articulos`
--

INSERT INTO `articulos` (`ID_ART`, `DESCRIPCION`, `UNIDAD`) VALUES
(1, 'puerta', 'pieza'),
(2, 'asiento', 'pieza'),
(3, 'llantas', 'pieza'),
(4, 'rines', 'pieza'),
(5, 'pintura', 'litro'),
(6, 'faros', 'pieza'),
(7, 'auto', 'pieza'),
(8, 'camioneta', 'pieza'),
(11, 'bocina', 'pieza'),
(12, 'foco', 'pieza'),
(13, 'cable', 'metro'),
(14, 'anticongelante', 'litro'),
(15, 'go-kart', 'pieza'),
(16, 'motor', 'pieza'),
(17, 'bujia', 'pieza'),
(18, 'motocicleta', 'pieza'),
(19, 'aleron', 'pieza'),
(20, 'aceite', 'litro');

-- --------------------------------------------------------

--
-- Table structure for table `articulo_estructura`
--

CREATE TABLE `articulo_estructura` (
  `ID_ART` int(11) NOT NULL,
  `ID_ESTRUCTURA` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articulo_estructura`
--

INSERT INTO `articulo_estructura` (`ID_ART`, `ID_ESTRUCTURA`) VALUES
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(15, 3),
(15, 4),
(18, 16),
(18, 17);

-- --------------------------------------------------------

--
-- Table structure for table `estructura`
--

CREATE TABLE `estructura` (
  `ID_ESTRUCTURA` int(11) NOT NULL,
  `ID_MATERIAL_PRIMARIO` int(11) DEFAULT NULL,
  `ID_MATERIAL_ALTERNO` int(11) DEFAULT NULL,
  `CANTIDAD` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estructura`
--

INSERT INTO `estructura` (`ID_ESTRUCTURA`, `ID_MATERIAL_PRIMARIO`, `ID_MATERIAL_ALTERNO`, `CANTIDAD`) VALUES
(1, 1, 0, 4),
(2, 0, 6, 2),
(3, 16, 0, 1),
(4, 0, 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ordenes`
--

CREATE TABLE `ordenes` (
  `ID_ART` int(11) NOT NULL,
  `ID_PROV` int(11) NOT NULL,
  `CANTIDAD` int(11) DEFAULT NULL,
  `UNIDAD` varchar(11) DEFAULT NULL,
  `IMPORTE` int(11) DEFAULT NULL,
  `F_ENTREGADA` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordenes`
--

INSERT INTO `ordenes` (`ID_ART`, `ID_PROV`, `CANTIDAD`, `UNIDAD`, `IMPORTE`, `F_ENTREGADA`) VALUES
(14, 1, 4, 'pieza', 0, 2),
(6, 3, 100, 'pieza', 300000, 2),
(14, 1, 4, 'pieza', 300000, 2),
(6, 3, 1, 'pieza', 0, 2),
(6, 3, 12, 'pieza', 36000, 0),
(6, 3, 1, 'pieza', 0, 0),
(6, 3, 10000, 'pieza', 30000000, 0),
(6, 3, 2, 'pieza', 6000, 0),
(6, 3, 11, 'pieza', 33000, 0),
(14, 1, 5, 'pieza', 700, 0),
(0, 0, 0, 'pieza', 0, 0),
(14, 1, 5, 'pieza', 700, 0),
(6, 3, 29, 'pieza', 87000, 0),
(6, 3, 12, 'pieza', 36000, 0),
(6, 3, 1, 'pieza', 0, 0),
(6, 3, 100000, 'pieza', 300000000, 0),
(6, 3, 100000, 'pieza', 300000000, 0),
(6, 3, 100000, 'pieza', 300000000, 3),
(6, 3, 1, 'pieza', 0, 3),
(14, 1, 400, 'pieza', 56000, 5),
(14, 1, 41, 'pieza', 5740, 5),
(6, 3, 1, 'pieza', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `provedores`
--

CREATE TABLE `provedores` (
  `ID_PROV` int(11) NOT NULL,
  `RAZON_SOCIAL` varchar(20) DEFAULT NULL,
  `NIVEL` int(20) DEFAULT NULL,
  `ESTATUS` char(2) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provedores`
--

INSERT INTO `provedores` (`ID_PROV`, `RAZON_SOCIAL`, `NIVEL`, `ESTATUS`) VALUES
(1, 'ABSA', 10, 'A'),
(2, 'CT', 7, 'A'),
(3, 'GRUPO W', 9, 'A'),
(4, 'MULTIPARTS', 10, 'I'),
(5, 'K&N', 10, 'A'),
(6, 'CLASH S.A. DE C.V', 9, 'A'),
(7, 'M&R', 8, 'A'),
(8, 'ROYALTY', 6, 'I'),
(9, 'METALLICO', 10, 'I');

-- --------------------------------------------------------

--
-- Table structure for table `prov_articulos`
--

CREATE TABLE `prov_articulos` (
  `ID_ART` int(11) DEFAULT NULL,
  `PRECIO` double DEFAULT NULL,
  `DIAS_ENTREGA` int(11) DEFAULT NULL,
  `ESTATUS` varchar(20) DEFAULT NULL,
  `ID_PROV` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prov_articulos`
--

INSERT INTO `prov_articulos` (`ID_ART`, `PRECIO`, `DIAS_ENTREGA`, `ESTATUS`, `ID_PROV`) VALUES
(5, 100, 2, 'A', 1),
(1, 500, 5, 'A', 1),
(2, 1000, 10, 'A', 1),
(3, 1500, 1, 'A', 1),
(4, 4000, 5, 'A', 1),
(11, 1500, 1, 'A', 1),
(12, 210, 5, 'A', 1),
(13, 100, 1, 'A', 1),
(14, 140, 5, 'A', 1),
(16, 55000, 5, 'A', 1),
(17, 120, 1, 'A', 1),
(19, 1350, 2, 'A', 1),
(20, 200, 1, 'A', 1),
(11, 1500, 1, 'A', 1),
(12, 210, 5, 'A', 1),
(13, 100, 1, 'A', 1),
(14, 140, 5, 'A', 1),
(16, 55000, 5, 'A', 1),
(17, 120, 1, 'A', 1),
(19, 1350, 2, 'A', 1),
(20, 200, 1, 'A', 1),
(11, 1300, 1, 'A', 2),
(12, 210, 5, 'A', 2),
(13, 110, 1, 'A', 2),
(14, 145, 3, 'A', 2),
(16, 50000, 5, 'A', 2),
(17, 150, 3, 'A', 2),
(19, 1550, 3, 'A', 2),
(20, 180, 2, 'A', 2),
(5, 500, 2, 'A', 3),
(6, 3000, 3, 'A', 3),
(11, 1300, 1, 'I', 3),
(12, 219, 5, 'A', 3),
(13, 111, 1, 'A', 3),
(14, 155, 2, 'I', 3),
(16, 49000, 5, 'A', 3),
(17, 120, 4, 'A', 3),
(19, 1500, 3, 'A', 3),
(20, 130, 2, 'A', 3),
(5, 500, 2, 'A', 4),
(6, 3000, 3, 'A', 4),
(11, 1300, 1, 'I', 4),
(12, 219, 5, 'A', 4),
(13, 111, 1, 'A', 4),
(14, 155, 2, 'I', 4),
(16, 49000, 5, 'A', 4),
(17, 120, 4, 'A', 4),
(19, 1500, 3, 'A', 4),
(20, 130, 2, 'A', 4),
(5, 500, 2, 'A', 5),
(6, 2300, 3, 'A', 5),
(11, 1400, 1, 'I', 5),
(12, 219, 5, 'A', 5),
(13, 111, 1, 'A', 5),
(14, 125, 2, 'I', 5),
(16, 49000, 5, 'A', 5),
(17, 120, 4, 'A', 5),
(19, 1500, 3, 'A', 5),
(20, 130, 2, 'A', 5),
(11, 1100, 2, 'A', 6),
(12, 210, 5, 'A', 6),
(13, 100, 1, 'A', 6),
(14, 140, 5, 'A', 6),
(16, 55000, 5, 'A', 6),
(17, 130, 1, 'A', 6),
(19, 1340, 2, 'A', 6),
(20, 200, 1, 'A', 6),
(5, 400, 2, 'A', 7),
(6, 2900, 3, 'A', 7),
(11, 1200, 1, 'I', 7),
(12, 219, 5, 'A', 7),
(13, 111, 1, 'A', 7),
(14, 155, 2, 'I', 7),
(16, 49000, 5, 'A', 7),
(17, 120, 4, 'A', 7),
(19, 1500, 3, 'A', 7),
(20, 130, 2, 'A', 7),
(11, 1400, 1, 'A', 8),
(12, 220, 5, 'A', 8),
(13, 100, 1, 'A', 8),
(14, 130, 5, 'A', 8),
(16, 52000, 5, 'A', 8),
(17, 100, 1, 'A', 8),
(19, 1200, 4, 'A', 8),
(20, 300, 1, 'A', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `almacen`
--
ALTER TABLE `almacen`
  ADD KEY `ID_ART` (`ID_ART`);

--
-- Indexes for table `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`ID_ART`);

--
-- Indexes for table `articulo_estructura`
--
ALTER TABLE `articulo_estructura`
  ADD KEY `ID_ART` (`ID_ART`),
  ADD KEY `ID_ESTRUCTURA` (`ID_ESTRUCTURA`);

--
-- Indexes for table `estructura`
--
ALTER TABLE `estructura`
  ADD PRIMARY KEY (`ID_ESTRUCTURA`);

--
-- Indexes for table `ordenes`
--
ALTER TABLE `ordenes`
  ADD KEY `ID_ART` (`ID_ART`),
  ADD KEY `ID_PROV` (`ID_PROV`);

--
-- Indexes for table `provedores`
--
ALTER TABLE `provedores`
  ADD KEY `ID_PROV` (`ID_PROV`);

--
-- Indexes for table `prov_articulos`
--
ALTER TABLE `prov_articulos`
  ADD KEY `ID_PROV` (`ID_PROV`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articulos`
--
ALTER TABLE `articulos`
  MODIFY `ID_ART` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `estructura`
--
ALTER TABLE `estructura`
  MODIFY `ID_ESTRUCTURA` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `provedores`
--
ALTER TABLE `provedores`
  MODIFY `ID_PROV` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;